from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from posts.models import Account, Posts, UserFollowing
# Create your views here.

#I need a view to create the account "home" page
class AccountCreateView(CreateView):
    model = Account
    template_name = "posts/createAccount.html"
    fields = ["name", "profile_picture", "bio"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("homepage")


#I need a view to show my account details and list my posts
class AccountDetailView(DetailView):
    model = Account
    template_name = "posts/list.html"
    context_object_name = "accountdetail"

    


#I need a view to create Posts
class PostsCreateView(CreateView):
    model = Posts
    template_name = "posts/createPost.html"
    fields = ["title", "image", "video", "description", "date_created"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.poster = self.request.user
        item.account = self.request.user.accounts
        item.save()
        return redirect("homepage")


class PostsDetailView(DetailView):
    model = Posts
    template_name = "posts/postDetail.html"
    context_object_name = "postdetail"