from django.urls import path
from posts.views import AccountCreateView, AccountDetailView, PostsCreateView, PostsDetailView

urlpatterns = [
    path("<int:pk>/", AccountDetailView.as_view(), name="homepage"),
    path("createaccount/", AccountCreateView.as_view(), name="create_account"),
    path("createpost/", PostsCreateView.as_view(), name="create_post"),
    path("post<int:pk>/", PostsDetailView.as_view(), name="post_detail")


]