from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.

class Account(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(User, related_name="accounts", on_delete=models.CASCADE)
    bio = models.TextField(null=True, blank=True)
    profile_picture = models.ImageField(upload_to="profile/", blank=True, null=True)


class Posts(models.Model):
    title = models.CharField(max_length=500)
    image = models.ImageField(upload_to="imagePosts/", blank=True, null=True)
    video = models.FileField(upload_to="videoPosts/", blank=True, null=True)
    description = models.TextField(null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    poster = models.ForeignKey(User, related_name="posts", on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name="posts", on_delete=models.CASCADE)


class UserFollowing(models.Model):
    user_id = models.ForeignKey(User, related_name="following", on_delete=models.CASCADE)
    following_user_id = models.ForeignKey(User, related_name="followers", on_delete=models.CASCADE)


