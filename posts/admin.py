from django.contrib import admin
from posts.models import Account, Posts, UserFollowing
# Register your models here.

class AccountAdmin(admin.ModelAdmin):
    pass

class PostsAdmin(admin.ModelAdmin):
    pass

class UserFollowingAdmin(admin.ModelAdmin):
    pass


admin.site.register(Account, AccountAdmin)
admin.site.register(Posts, PostsAdmin)
admin.site.register(UserFollowing, UserFollowingAdmin)

